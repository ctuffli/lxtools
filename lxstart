#!/bin/sh
#
# Copyright (c) 2018 Chuck Tuffli <chuck@tuffli.net>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#
# Start a Linux container
#   usage: lxstart -d c7	# runs login
#          lxstart -d c7 -r	# runs bash with user root
#
# Note, portions of the script needs superuser privileges
#
# The default location for the LXC_LOCATION is /linux
# Run with env LXC_LOCATION=/foo/bar/baz lxstart to override

ROOT_SH=0

LXC_LOCATION=${LXC_LOCATION:="/linux"}

if [ $(id -u) -ne 0 ]
then
	echo "Must run as root"
	exit
fi

args=`getopt d:r $*`
if [ $? -ne 0 ]
then
	echo 'Usage lxstart -d distribution [-r]'
	exit 2
fi
set -- $args
while :
do
	case "$1" in
		-d)
			# which distribution
			LXC_LOCATION=${LXC_LOCATION}/$2
			shift; shift;
			;;
		-r)
			# Start a shell as root
			ROOT_SH=1
			shift
			;;
		--)
			shift; break
			;;
	esac
done

# Make really supper doubly sure $LXC_LOCATION is a thing and not empty
if [ -z "$LXC_LOCATION" ] || [ ! -d $LXC_LOCATION ] || [ "$LXC_LOCATION" == "/" ]
then
	echo "Bad value for LXC_LOCATION=\"$LXC_LOCATION\""
	exit 1
fi

# Load the compat modules
kldload linux_common linux64 linprocfs 2> /dev/null

sysctl compat.linux.osrelease=3.10.0

# Ensure linprocfs is mounted
PROCFS_MOUNT=`mount -t linprocfs -p | awk '$0~dir {print $2}' dir="$LXC_LOCATION/proc"`
if [ "$LXC_LOCATION/proc" != "$PROCFS_MOUNT" ]
then
	if [ ! -d $LXC_LOCATION/proc ]
	then
		mkdir $LXC_LOCATION/proc
	fi

	echo "Mounting linprocfs ..."
	mount -t linprocfs linproc $LXC_LOCATION/proc
fi

# Ensure devfs is mounted
DEVFS_MOUNT=`mount -t devfs -p | awk '$0~dir {print $2}' dir="$LXC_LOCATION/dev"`
if [ "$LXC_LOCATION/dev" != "$DEVFS_MOUNT" ]
then
	if [ ! -d $LXC_LOCATION/dev ]
	then
		mkdir $LXC_LOCATION/dev
	fi

	echo "Mounting devfs ..."
	mount -t devfs devfs $LXC_LOCATION/dev

	# Add Linux fdescfs compatibility
	echo "Mounting fdescfs ..."
	mount -t fdescfs -o linrdlnk null $LXC_LOCATION/dev/fd
fi

if [ $ROOT_SH -eq 1 ]
then
	/usr/sbin/chroot -u root ${LXC_LOCATION} env -i HOME=/root /bin/bash --login
else
	# login needs a separate process (?)
	set -o monitor
	/usr/sbin/chroot ${LXC_LOCATION} /usr/bin/login -p
fi

echo -n "Cleaning up ..."

umount $LXC_LOCATION/dev/fd

umount $LXC_LOCATION/dev

umount $LXC_LOCATION/proc

# Unload the compat modules
kldunload fdescfs linprocfs linux64 linux_common 2> /dev/null

echo

exit 0
