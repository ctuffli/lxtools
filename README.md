Linuxulator Tools
=================

The Linuxulator Tools help create and run Linux based chroot
environments on FreeBSD systems.

## lxcreate

lxcreate initializes a Linux environment by extracting the tar-ball from
the Docker image. By default, it extract images under `/linux/<distro>`,
but the location can be modified by setting the `LXC_LOCATION`
environment variable. Note that the script does not download the image
itself. Some common images are:

* CentOS 7.4 : https://github.com/CentOS/sig-cloud-instance-images/blob/CentOS-7.4.1708/docker/centos-7.4.1708-docker.tar.xz
* Ubuntu 14.04 : https://partner-images.canonical.com/core/trusty/current/ubuntu-trusty-core-cloudimg-amd64-root.tar.gz

By default, lxcreate looks for these image files in the user's downloads
directory (e.g. `$HOME/Downloads/`).

The command `lxcreate centos7` will create a CentOS 7 environment under
`/linux/c7/`.

## lxstart

`lxstart` starts the specified Linux environment using the Linuxulator
components and [chroot(8)][chroot_man]. `lxstart` requires the distribution as a
parameter (e.g. `c7` for CentOS 7) and be default will run [login(1)][login_man].
Initially, there are no users and most distributions prevent login as
root. To start a bash shell as root, add the `-r` option to `lxstart`.
It is useful to create a user with the same UID and GID as the host OS
to simplify file ownership issues. Other "quality of life" hacks for
the user account are:
* `passwd -u `_user_
* `passwd -d `_user_
* `gpasswd -a `_user_` wheel`

This unlocks the user account, deletes the password, and adds the user
to the group wheel to allow sudo'ing. Note that It may be useful to
mount your home directory inside the environment using [nullfs(5)][nullfs_man]. E.g.:

`# mount_nullfs $HOME /linux/c7/$HOME`

[chroot_man]:https://www.freebsd.org/cgi/man.cgi?query=chroot&apropos=0&sektion=0&manpath=FreeBSD+11.2-RELEASE+and+Ports&arch=default&format=html
[login_man]:http://man7.org/linux/man-pages/man1/login.1.html
[nullfs_man]:https://www.freebsd.org/cgi/man.cgi?query=nullfs&apropos=0&sektion=0&manpath=FreeBSD+11.2-RELEASE+and+Ports&arch=default&format=html
